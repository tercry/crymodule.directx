#include "DXMain.h"
#include "DXInternal.h"
#include <CryModule/Class.h>
#include <IRenderer.h>

LPDIRECT3DTEXTURE9 g_pTextTexture = NULL;

ITexPic *CreateTexture(int w, int h, char *szCacheName)
{
	ITexPic *pTexPic;

	unsigned char *pBuffer = new unsigned char[w * h * 4];

	int iTextureID = CryModule::aSystem.Get<IRenderer>()->DownLoadToVideoMemory(pBuffer, w, h,
		eTF_8888, eTF_8888, 0, 0, FILTER_LINEAR, 0, szCacheName, FT_DYNAMIC | FT2_RENDERTARGET);


	if (iTextureID > -1)
	{
		pTexPic = CryModule::aSystem.Get<IRenderer>()->EF_GetTextureByID(iTextureID);	
	}
	else
	{
		pTexPic = NULL;
	}

	delete [] pBuffer;

	return pTexPic;
}

LPDIRECT3DTEXTURE9 GetTexture(ITexPic *pTexPic)
{
	return (LPDIRECT3DTEXTURE9 )DX::Internal::GetItemInObject<void*, uintptr_t>(pTexPic, 24); 
}
LPDIRECT3DDEVICE9EX GetDevice(IRenderer* pRenderer)
{
	return (LPDIRECT3DDEVICE9EX)pRenderer->EF_Query(EFQ_D3DDevice);
}
LPDIRECT3DDEVICE9EX GetDevice()
{
	return GetDevice(CryModule::aSystem.Get<IRenderer>());
}

const char *GetD3DErrorType(HRESULT aType)
{
#define c(n) if (aType == n) return #n;
	
	
c(D3D_OK);                              

c(D3DERR_WRONGTEXTUREFORMAT               );
c(D3DERR_UNSUPPORTEDCOLOROPERATION        );
c(D3DERR_UNSUPPORTEDCOLORARG              );
c(D3DERR_UNSUPPORTEDALPHAOPERATION        );
c(D3DERR_UNSUPPORTEDALPHAARG              );
c(D3DERR_TOOMANYOPERATIONS                );
c(D3DERR_CONFLICTINGTEXTUREFILTER         );
c(D3DERR_UNSUPPORTEDFACTORVALUE           );
c(D3DERR_CONFLICTINGRENDERSTATE           );
c(D3DERR_UNSUPPORTEDTEXTUREFILTER         );
c(D3DERR_CONFLICTINGTEXTUREPALETTE        );
c(D3DERR_DRIVERINTERNALERROR              );

c(D3DERR_NOTFOUND                         );
c(D3DERR_MOREDATA                         );
c(D3DERR_DEVICELOST                       );
c(D3DERR_DEVICENOTRESET                   );
c(D3DERR_NOTAVAILABLE                     );
c(D3DERR_OUTOFVIDEOMEMORY                 );
c(D3DERR_INVALIDDEVICE                    );
c(D3DERR_INVALIDCALL                      );
c(D3DERR_DRIVERINVALIDCALL                );
c(D3DERR_WASSTILLDRAWING                  );
c(D3DOK_NOAUTOGEN                         );


	return NULL;

#undef c
}


