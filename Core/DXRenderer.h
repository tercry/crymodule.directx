#pragma once

namespace DX
{
	class CRenderer
	{
	public:
		CRenderer();
		~CRenderer();

		bool Init(void *pD3Device, const char *szRootPath = NULL);
		void Shutdown();
		void Clear(unsigned char uR = 0,unsigned char uG = 0,unsigned char uB = 0);
		unsigned int GetSimultaneousRT(unsigned int uRequiredRT = 0, unsigned int *pRT = 0, unsigned int *pPasses = 0);

		void Test();
		bool IsValidTexture(void *pTexture);
		void GetTextureWithID(size_t uID, void **pTexture);
		void SetTextureWithID(size_t uID, void *pTexture);
		void TestSetTexture(void *pTexture);
		void *GetTextureWithOffset(void *pObject, size_t uOffset);
	private:
		
		bool FixPath(wchar_t *wBuffer, const wchar_t *szPath);

		bool LoadTexure(const wchar_t *szPath, IDirect3DTexture9 **pTexture);

		void InitGeo();
		void SetupMatrices();
		void SetupLights();
		void SetupTextures();
		void SetupRenderer();

		wchar_t m_swzRootPath[MAX_PATH];
		IDirect3DDevice9 *m_pd3dDevice;
	};
}
