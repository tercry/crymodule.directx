#pragma once
#define DXLIB
namespace DX
{
	struct IRenderer
	{
		virtual bool Init(void *pD3Device, const char *szRootPath = 0) = 0;
		virtual void Shutdown() = 0;
		virtual void Clear(unsigned char uR = 0,unsigned char uG = 0,unsigned char uB = 0) = 0;
		virtual unsigned int GetSimultaneousRT(unsigned int uRequiredRT = 0, unsigned int *pRT = 0, unsigned int *pPasses = 0) = 0;
		virtual void Test() = 0;
		virtual bool IsValidTexture(void *pTexture) = 0;
		virtual void GetTextureWithID(size_t uID, void **pTexture) = 0;
		virtual void SetTextureWithID(size_t uID, void *pTexture) = 0;
		virtual void TestSetTexture(void *pTexture) = 0;
		virtual void *GetTextureWithOffset(void *pObject, size_t uOffset) = 0;
	};

	extern IRenderer* Create();
}
