#pragma once

#include <windows.h>

namespace DX
{
	namespace Internal
	{
		/***************************/
		/* ansi-unicode conversion */
		/***************************/
		BOOL AnsiToUnicode16(CHAR *in_Src, WCHAR *out_Dst, INT in_MaxLen);
		BOOL AnsiToUnicode16L(CHAR *in_Src, INT in_SrcLen, WCHAR *out_Dst, INT in_MaxLen);
		bool FixPath(wchar_t *wBuffer, const wchar_t *szRootPath, const wchar_t *szPath);

		/***************************/
		/* templates */
		/***************************/
	
		template <typename TItem, typename TBlock> TItem GetItemInObject(void *pObject, size_t uOffset)
		{
			return (TItem)(*((TBlock*)pObject + (uOffset / sizeof(TBlock))));
		}


	}
}