#pragma warning(disable: 4995)
#pragma warning(disable: 4311)

#include "RenderTargetSurface.h"

#include <CryModule/Class.h>
#include <IRenderer.h>

namespace Renderer
{
	static CRenderTargetSurface::TStorage* s_pStorage = NULL;

    CRenderTargetSurface::TStorage* CRenderTargetSurface::GetStorage(IRenderer *pRenderer)
    {
        if (!s_pStorage)
        {
            s_pStorage = (TStorage*)((size_t)pRenderer + 119944);
        }

        return s_pStorage;
    }
    
    CRenderTargetSurface* CRenderTargetSurface::Create(int w, int h)
    {
        IRenderer* pRenderer = CryModule::aSystem.Get<IRenderer>();

        return &s_pStorage->Get(pRenderer->CreateRenderTarget(w, h, eTF_8888));
    }
    
    bool CRenderTargetSurface::IsValid()
    {
        if (this == NULL || m_pColor == NULL || m_pDepth == NULL) return false;

        IDirect3DDevice9 *pRendererDevice = GetDevice();
        IDirect3DDevice9 *pSurfaceDevice = NULL;

        m_pColor->GetDevice(&pSurfaceDevice);

        if (pSurfaceDevice != pRendererDevice) return false;

        m_pDepth->GetDevice(&pSurfaceDevice);

        return pSurfaceDevice == pRendererDevice;
    }
    
    bool CRenderTargetSurface::Begin(
        CONST D3DCOLOR&     krClearColor, 
        D3DVIEWPORT9*       pViewport, 
        D3DVIEWPORT9*       pViewportForRestoration,
        bool                bCopyViewportZRange
    ) {
        IRenderer* pRenderer = CryModule::aSystem.Get<IRenderer>();
        IDirect3DDevice9Ex* pDevice = GetDevice(pRenderer);

        if (pViewportForRestoration != NULL)
        {
			pDevice->GetViewport(pViewportForRestoration); 
        }

        if (pRenderer->SetRenderTarget(GetHandler(pRenderer)))
        {
            if (pViewport != NULL)
            {
                if (bCopyViewportZRange && pViewportForRestoration != NULL)
                {
                    pViewport->MinZ = pViewportForRestoration->MinZ;
                    pViewport->MaxZ = pViewportForRestoration->MaxZ;
                }

                pDevice->SetViewport(pViewport);
            }

			pDevice->Clear(0, NULL, D3DCLEAR_ZBUFFER | D3DCLEAR_TARGET, krClearColor, 1.0f, 0);

            return true;
        }

        return false;
    }

    bool CRenderTargetSurface::End(
        CONST D3DVIEWPORT9* kpViewportToRestore
    ) {
        IRenderer* pRenderer = CryModule::aSystem.Get<IRenderer>();
        IDirect3DDevice9Ex* pDevice = GetDevice(pRenderer);

        if (kpViewportToRestore != NULL)
        {
            pDevice->SetViewport(kpViewportToRestore);
        }

        return pRenderer->SetRenderTarget(0);
    }

    void CRenderTargetSurface::Copy(IDirect3DSurface9* pSurface, bool bDepth)
    {
		CopySurface(*(&m_pColor + bDepth), pSurface);
    }

    int CRenderTargetSurface::GetHandler()
    {
        return GetHandler(CryModule::aSystem.Get<IRenderer>());
    }
    int CRenderTargetSurface::GetHandler(IRenderer* pRenderer)
    {
        return (size_t*)this - (size_t*)&GetStorage(pRenderer)->Get(0);
    }
}