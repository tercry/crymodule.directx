#pragma once

#include "DXMain.h"
#include <Tarray.h>

struct IRenderer;

namespace Renderer
{
    class CRenderTargetSurface
    {
    public:
        typedef TArray<CRenderTargetSurface> TStorage;
    public:
        static TStorage* GetStorage(IRenderer *pRenderer = NULL);
        static CRenderTargetSurface* Create(int w, int h);
    public:
        

		bool IsValid();

        bool Begin(
            CONST D3DCOLOR&     krClearColor, 
            D3DVIEWPORT9*       kViewport = NULL, 
            D3DVIEWPORT9*       pViewportForRestoration = NULL,
            bool                bCopyViewportZRange = false
        );
        bool End(
            CONST D3DVIEWPORT9* kpViewportToRestore = NULL
        );

        void Copy(IDirect3DSurface9* pSurface, bool bDepth = false);

        int GetHandler();
    private:
        int GetHandler(IRenderer* pRenderer);
    private:
        IDirect3DSurface9* m_pColor;
        IDirect3DSurface9* m_pDepth;
    };
}
