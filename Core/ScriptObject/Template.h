#pragma once

#include <_ScriptableEx.h>

namespace ScriptObject
{
	template <typename T> class CTemplate : public _ScriptableEx<T>
	{
	public:
	    CTemplate();

		static void InitTemplate(IScriptSystem *pSS);
        static void ReleaseTemplate();
        
		static IScriptObject* Create(IFunctionHandler *pH);
        
        static ::Script::ObjectThunk *GetObjectThunk();
	};
}