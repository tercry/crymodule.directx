#include "Target.h"
#include "Template.inl"

namespace ScriptObject
{
	CTarget::CTarget() : 
        m_pRenderTarget(0),
        m_pRenderTargets(0),
        m_pRenderer(0),
        m_pTexture(0), 
        m_pTextureSurface(0)
    { }

    CTarget::CTarget(const CTarget &krc) : 
        m_pRenderTarget(krc.m_pRenderTarget),
        m_pRenderTargets(krc.m_pRenderTargets),
        m_pRenderer(krc.m_pRenderer),
        m_pTexture(krc.m_pTexture), 
        m_pTextureSurface(krc.m_pTextureSurface),
        m_cDelegates(krc.m_cDelegates)
    { }

    void CTarget::Prepare()
    {
        SAFE_RELEASE(m_pTextureSurface);

        if (m_pTexture == NULL)
        {
            
        }
        else
        {
            GetTexture(m_pTexture)->GetSurfaceLevel(0, &m_pTextureSurface);
            
            m_cViewport.X = m_cViewport.Y = 0;

            m_cViewport.Width = m_pTexture->GetWidth();
            m_cViewport.Height = m_pTexture->GetHeight();
        }
    }


    void CTarget::Register(Utils::Plugin::IXKernel *pKernel)
    {
        TType::Register(pKernel);
        
        m_pRenderer = CryModule::aSystem.Get<IRenderer>();
        m_pRenderTargets = Renderer::CRenderTargetSurface::GetStorage(m_pRenderer);

        SetPrecacheResourcesMode(false);
    }

	void CTarget::SetPrecacheResourcesMode(bool bEnable)
    {
        if (bEnable)
        {
            Prepare();
        }
        else
        {
            SAFE_RELEASE(m_pTextureSurface);
        }
    }
    
	CryModule::IRunLoop::eMode CTarget::GetMode() 
    {
        return eMode_UnPaused;
    }

    CryModule::IRunLoop::eUpdate CTarget::GetUpdate()
    {
        return eUpdate_System;
    }
    

    bool CTarget::Update(CryModule::IRunLoop::eUpdate update, bool bAfter)
    {
        RenderDelegates();

        return true;
    }


    int CTarget::Init(IFunctionHandler *pH)
    {
        return pH->EndFunctionNull();
    }
    int CTarget::Clear(IFunctionHandler *pH)
    {
        return pH->EndFunctionNull();
    }
    int CTarget::Release(IFunctionHandler *pH)
    {
        ProcessDelegates(&CTarget::ReleaseDelegate);

        m_cDelegates.clear();

        return pH->EndFunctionNull();
    }
    int CTarget::SetTextureID(IFunctionHandler *pH)
    {
        int iCookie = 0;
        INT_PTR iTextureID = -1;

        pH->GetParamUDVal(1, iTextureID, iCookie);
        
        SAFE_RELEASE(m_pTextureSurface);

        if (iTextureID < 0)
        {
            m_pTexture = NULL;
        }
        else
        {
            m_pTexture = m_pRenderer->EF_GetTextureByID(iTextureID);

            Prepare();
            
            if (m_pTexture)
            {
                m_pRenderTarget = Renderer::CRenderTargetSurface::Create(m_pTexture->GetWidth(), m_pTexture->GetHeight());
            }
        }

        return pH->EndFunctionNull();
    }
    int CTarget::GetTextureID(IFunctionHandler *pH)
    {
        if (m_pTexture == NULL)
        {
            return pH->EndFunctionNull();
        }

        int tid = (int)m_pTexture->GetTextureID();

        USER_DATA pUserData = m_pScriptSystem->CreateUserData(tid, USER_DATA_TEXTURE);
        
        return pH->EndFunction(pUserData);
    }

    int CTarget::RegisterDelegate(IFunctionHandler *pH)
    {
        m_cDelegates.push_back(CScriptFunctionDelegate());
        
        m_cDelegates.back().Set(pH);

        return pH->EndFunctionNull();
    }

    void CTarget::InitTemplate(IScriptSystem *pSS)
    {
        CTemplate<CTarget>::InitializeTemplate(pSS);

        REG_FUNC(CTarget, Init);
        REG_FUNC(CTarget, Clear);
        REG_FUNC(CTarget, Release);
        REG_FUNC(CTarget, SetTextureID);
        REG_FUNC(CTarget, GetTextureID);
        REG_FUNC(CTarget, RegisterDelegate);
    }

    IScriptObject* CTarget::GetClassScriptObject()
    {
        return m_pScriptThis;
    }
    
    bool CTarget::RenderDelegates()
    {
        if (m_pTexture && 
			m_pTextureSurface &&
			m_pRenderTarget &&
            m_pRenderTarget->Begin(0xFF0000FF, &m_cViewport, &m_cRestoreViewport, true)
        ) {
            ProcessDelegates(&CTarget::InvokeDelegate);

            m_pRenderTarget->End(&m_cRestoreViewport);
            m_pRenderTarget->Copy(m_pTextureSurface, false);
        }

        return true;
    }
    
    void CTarget::InvokeDelegate(CScriptFunctionDelegate& aDelegate)
    {
        aDelegate.Call();
    }

    void CTarget::ReleaseDelegate(CScriptFunctionDelegate& aDelegate)
    {
        aDelegate.Release();
    }


    void CTarget::ProcessDelegates(void(CTarget::*pThunk)(CScriptFunctionDelegate&))
    {
        for(TDelegateVector::iterator it = m_cDelegates.begin(); it != m_cDelegates.end(); it++)
        {
            (this->*(pThunk))(*it);
        }
    }
}