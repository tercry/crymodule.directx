namespace ScriptObject
{
	template <typename T> CTemplate<T>::CTemplate()
    {
        Init(CryModule::aSystem.Get<IScriptSystem>(), (T*)this);
    }

    template <typename T> void CTemplate<T>::InitTemplate(IScriptSystem *pSS)
    {
        _ScriptableEx<T>::InitializeTemplate(pSS);
    }

    template <typename T> void CTemplate<T>::ReleaseTemplate()
    {
        _ScriptableEx<T>::ReleaseTemplate();
    }


    template <typename T> IScriptObject* CTemplate<T>::Create(IFunctionHandler *pH)
    {
        return Utils::Plugin::Class::aBundle.Register<T>(false)->GetScriptObject();
    }

    template <typename T> ::Script::ObjectThunk *CTemplate<T>::GetObjectThunk()
    {
        static ::Script::ObjectThunk aThunk = {false, &T::InitTemplate, &T::ReleaseTemplate, &T::Create, NULL};

        return &aThunk;
    }
}