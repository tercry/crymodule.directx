#pragma once

#include <CryModule/Class.h>

#include "Template.h"
#include <IRenderer.h>
#include <ScriptFunctionDelegate.h>
#include <vector>

#include "../Renderer/RenderTargetSurface.h"

namespace ScriptObject
{
	class CTarget :
        public CryModule::IRunLoop, 
        public CryModule::IPrecacheResourcesMode,
        public CryModule::Script::IClass,
        public CryModule::Class<CTarget, CTemplate>
    {
        typedef CScriptFunctionDelegate::TVector TDelegateVector;
        
    public:

		CTarget();

        CTarget(const CTarget &krc);

        void Register(Utils::Plugin::IXKernel *pKernel);

        static void InitTemplate(IScriptSystem *pSS);
        IScriptObject* GetClassScriptObject();

        MODULE_METHOD void SetPrecacheResourcesMode(bool bEnable);
        
        IRunLoop::eMode GetMode();
        IRunLoop::eUpdate GetUpdate();
        bool Update(IRunLoop::eUpdate update, bool bAfter);

        int Init(IFunctionHandler *pH);
        int Clear(IFunctionHandler *pH);
        int Release(IFunctionHandler *pH);
        int SetTextureID(IFunctionHandler *pH);
        int GetTextureID(IFunctionHandler *pH);
        int RegisterDelegate(IFunctionHandler *pH);
    private:
        void Prepare();
        
        bool RenderDelegates();
        void InvokeDelegate(CScriptFunctionDelegate& aDelegate);
        void ReleaseDelegate(CScriptFunctionDelegate& aDelegate);
        void ProcessDelegates(void(CTarget::*pThunk)(CScriptFunctionDelegate&));
    private:
		Renderer::CRenderTargetSurface*				m_pRenderTarget;
		Renderer::CRenderTargetSurface::TStorage*	m_pRenderTargets;

		IRenderer*									m_pRenderer;
		
		ITexPic*			m_pTexture;
		IDirect3DSurface9*	m_pTextureSurface;

		TDelegateVector m_cDelegates;
		
		D3DVIEWPORT9 m_cViewport;
		D3DVIEWPORT9 m_cRestoreViewport;
    };
}