#include "Target.h"
#include "Template.inl"

namespace ScriptObject
{
	class Registry : 
		public CryModule::Script::IFactory,
		public CryModule::Class<Registry>
	{
	public:
		CryModule::Script::SFactory *GetFactoryItems(size_t &nCount)
		{
			static CryModule::Script::SFactory aItems[] = 
			{
				CryModule::Script::SFactory(CTarget::GetObjectThunk(), "Renderer.Target", "Render Target Script Object")
			};

			nCount = sizeof(aItems) / sizeof(aItems[0]);

			return &aItems[0];
		}
	} UtilsPluginClassRegister(Registry);
}