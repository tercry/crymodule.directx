#include "DXInternal.h"
#include <cstdio>
namespace DX
{
	namespace Internal
	{	
		
		/***************************/
		/* ansi-unicode conversion */
		/***************************/

		BOOL AnsiToUnicode16(CHAR *in_Src, WCHAR *out_Dst, INT in_MaxLen)
		{
			/* locals */
			INT lv_Len;

			// do NOT decrease maxlen for the eos
			if (in_MaxLen <= 0)
				return FALSE;

			// let windows find out the meaning of ansi
			// - the SrcLen=-1 triggers MBTWC to add a eos to Dst and fails if MaxLen is too small.
			// - if SrcLen is specified then no eos is added
			// - if (SrcLen+1) is specified then the eos IS added
			lv_Len = MultiByteToWideChar(CP_ACP, 0, in_Src, -1, out_Dst, in_MaxLen);

			// validate
			if (lv_Len < 0)
				lv_Len = 0;

			// ensure eos, watch out for a full buffersize
			// - if the buffer is full without an eos then clear the output like MBTWC does
			//   in case of too small outputbuffer
			// - unfortunately there is no way to let MBTWC return shortened strings,
			//   if the outputbuffer is too small then it fails completely
			if (lv_Len < in_MaxLen)
				out_Dst[lv_Len] = 0;
			else if (out_Dst[in_MaxLen-1])
				out_Dst[0] = 0;

			// done
			return TRUE;
		}


		BOOL AnsiToUnicode16L(CHAR *in_Src, INT in_SrcLen, WCHAR *out_Dst, INT in_MaxLen)
		{
			/* locals */
			INT lv_Len;


			// do NOT decrease maxlen for the eos
			if (in_MaxLen <= 0)
				return FALSE;

			// let windows find out the meaning of ansi
			// - the SrcLen=-1 triggers MBTWC to add a eos to Dst and fails if MaxLen is too small.
			// - if SrcLen is specified then no eos is added
			// - if (SrcLen+1) is specified then the eos IS added
			lv_Len = MultiByteToWideChar(CP_ACP, 0, in_Src, in_SrcLen, out_Dst, in_MaxLen);

			// validate
			if (lv_Len < 0)
				lv_Len = 0;

			// ensure eos, watch out for a full buffersize
			// - if the buffer is full without an eos then clear the output like MBTWC does
			//   in case of too small outputbuffer
			// - unfortunately there is no way to let MBTWC return shortened strings,
			//   if the outputbuffer is too small then it fails completely
			if (lv_Len < in_MaxLen)
				out_Dst[lv_Len] = 0;
			else if (out_Dst[in_MaxLen-1])
				out_Dst[0] = 0;

			// done
			return TRUE;
		}
		
		bool FixPath(wchar_t *wBuffer, const wchar_t *szRootPath, const wchar_t *szPath)
		{
				
			const bool bFixed = szRootPath[0] != NULL;

			if (!bFixed)
			{
				swprintf(wBuffer, L"%s", szPath);
			}
			else
			{
				swprintf(wBuffer, L"%s%s", szRootPath, szPath);
			}

			return bFixed;
		}
	}
}

/*
std::string string_to_hex(const std::string& input)
{
    static const char* const lut = "0123456789ABCDEF";
    size_t len = input.length();

    std::string output;
    output.reserve(2 * len);
    for (size_t i = 0; i < len; ++i)
    {
        const unsigned char c = input[i];
        output.push_back(lut[c >> 4]);
        output.push_back(lut[c & 15]);
    }
    return output;
}

#include <algorithm>
#include <stdexcept>

std::string hex_to_string(const std::string& input)
{
    static const char* const lut = "0123456789ABCDEF";
    size_t len = input.length();
    if (len & 1) throw std::invalid_argument("odd length");

    std::string output;
    output.reserve(len / 2);
    for (size_t i = 0; i < len; i += 2)
    {
        char a = input[i];
        const char* p = std::lower_bound(lut, lut + 16, a);
        if (*p != a) throw std::invalid_argument("not a hex digit");

        char b = input[i + 1];
        const char* q = std::lower_bound(lut, lut + 16, b);
        if (*q != b) throw std::invalid_argument("not a hex digit");

        output.push_back(((p - lut) << 4) | (q - lut));
    }
    return output;
}
*/