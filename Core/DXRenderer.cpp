
#include <CryModule/Class.h>

#include <IRenderer.h>
#include "DXMain.h"

IDirect3DDevice9 *g_pd3dDevice = NULL;

LPDIRECT3D9             g_piD3D       = NULL; // Used to create the D3DDevice
LPDIRECT3DDEVICE9       g_pid3dDevice = NULL; // Our rendering device

LPDIRECT3DTEXTURE9      g_pTexture   = NULL; // Our texture
LPDIRECT3DVERTEXBUFFER9 g_pVB        = NULL; // Buffer to hold vertices
// A structure for our custom vertex type. We added texture coordinates
struct CUSTOMVERTEX
{
    D3DXVECTOR3 position; // The position
    D3DXVECTOR3 normal;    // The normal
    FLOAT       tu, tv;   // The texture coordinates
};

// Our custom FVF, which describes our custom vertex structure
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1)


//-----------------------------------------------------------------------------
// Name: InitD3D()
// Desc: Initializes Direct3D
//-----------------------------------------------------------------------------
HRESULT InitD3D( HWND hWnd )
{
    // Create the D3D object.
    if( NULL == ( g_piD3D = Direct3DCreate9( D3D_SDK_VERSION ) ) )
        return E_FAIL;

    // Set up the structure used to create the D3DDevice
    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory( &d3dpp, sizeof(d3dpp) );
    d3dpp.Windowed = TRUE;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;

    // Create the D3DDevice
    if( FAILED( g_piD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
                                      D3DCREATE_SOFTWARE_VERTEXPROCESSING,
                                      &d3dpp, &g_pid3dDevice ) ) )
    {
        return E_FAIL;
    }

    // Device state would normally be set here

    return S_OK;
}


namespace DX
{
	class CRenderer : 
		public CryModule::IPrecacheResourcesMode, 
		public CryModule::IRunLoop,
		public CryModule::Class<CRenderer>
	{
	public:

		CRenderer()
		{
			bool b;

			b = false;

			b = true;
		}
		
		
		void Register(Utils::Plugin::IXKernel *pKernel)
		{
			TType::Register(pKernel);
			
			SetPrecacheResourcesMode(false);
		}

		~CRenderer()
		{
			Shutdown();
		}


		eMode GetMode() 
		{
			return eMode_UnPaused;
		}

		eUpdate GetUpdate()
		{
			return eUpdate_Flush;
		}
		
		bool Update(eUpdate update, bool bAfter)
		{
			//Test();

			return true;
		}


		bool Init(void *pD3Device, const char *szRootPath = NULL)
		{
			std::string sRootPath;
			
			if (szRootPath)
			{
				sRootPath += "Mods\\";
				sRootPath += szRootPath;
				sRootPath += "\\";

				szRootPath = sRootPath.c_str();
			}

			g_pd3dDevice = (IDirect3DDevice9*)pD3Device;
			g_pid3dDevice = g_pd3dDevice;
			//InitD3D((HWND)CryModule::aSystem.Get<IRenderer>()->GetHWND());

			Internal::AnsiToUnicode16((char*)szRootPath, m_swzRootPath, MAX_PATH);
			
			g_pVB = NULL;

			return g_pd3dDevice != NULL;
		}
		void Shutdown()
		{
			g_pd3dDevice = NULL;
			SAFE_RELEASE(g_pTexture);
			SAFE_RELEASE(g_pVB);
		}
		void Clear(unsigned char uR = 0,unsigned char uG = 0,unsigned char uB = 0)
		{
			g_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_ARGB(0, uR, uG, uB), 1.0f, 0);
		}

		unsigned int GetSimultaneousRT(unsigned int uRequiredRT = 0, unsigned int *pRT = 0, unsigned int *pPasses = 0)
		{
			D3DCAPS9 Caps;

			g_pd3dDevice->GetDeviceCaps( &Caps );
			
			unsigned int uNum = Caps.NumSimultaneousRTs;
			
			if (uRequiredRT > 0 && pRT != NULL && pPasses != NULL)
			{
				if (uNum > uRequiredRT)
				{
					uNum = uRequiredRT;
				}
				else if (uNum < 1)
				{
					uNum = 1;
				}

				(*pPasses) = (1 + uRequiredRT) - uNum;
				(*pRT) = uNum;
			}

			return uNum;
		}

		void Test()
		{
	#if 1
			if (!g_pTextTexture || !g_pd3dDevice) return;

			struct DXState {

				D3DLIGHT9 light;
				D3DMATERIAL9 mtrl;

				DWORD dwRenderState[4]; //7-60=53, 128-209=81, =134
				DWORD dwFVF;

				LPDIRECT3DVERTEXBUFFER9 pVB;
				UINT uOffset;
				UINT uStride;
				
				D3DXMATRIXA16 mat[3];

				DWORD dwTextureStageState[4];
				IDirect3DBaseTexture9 * pTexture;

				void Get(IDirect3DDevice9 *pd3dDevice)
				{
					
					pd3dDevice->GetRenderState( D3DRS_LIGHTING, &dwRenderState[0] );
					pd3dDevice->GetRenderState( D3DRS_AMBIENT, &dwRenderState[1] );
					pd3dDevice->GetRenderState( D3DRS_CULLMODE, &dwRenderState[2] );
					pd3dDevice->GetRenderState( D3DRS_ZENABLE, &dwRenderState[3] );

					pd3dDevice->GetLight(0, &light);
					pd3dDevice->GetMaterial(&mtrl);

					pd3dDevice->GetFVF(&dwFVF);
					
					pd3dDevice->GetStreamSource(0, &pVB, &uOffset, &uStride);
					
					pd3dDevice->GetTransform(D3DTS_WORLD, &mat[0]);
					pd3dDevice->GetTransform(D3DTS_VIEW, &mat[1]);
					pd3dDevice->GetTransform(D3DTS_PROJECTION, &mat[2]);

					pTexture = NULL;

					pd3dDevice->GetTexture( 0, &pTexture );
					if (pTexture != NULL)
					{
						pd3dDevice->GetTextureStageState( 0, D3DTSS_COLOROP,   &dwTextureStageState[0] );
						pd3dDevice->GetTextureStageState( 0, D3DTSS_COLORARG1, &dwTextureStageState[1]  );
						pd3dDevice->GetTextureStageState( 0, D3DTSS_COLORARG2, &dwTextureStageState[2]  );
						pd3dDevice->GetTextureStageState( 0, D3DTSS_ALPHAOP,   &dwTextureStageState[3]  );
					}
				}
				
				void Set(IDirect3DDevice9 *pd3dDevice)
				{
					if (pTexture != NULL)
					{
						pd3dDevice->SetTexture( 0, pTexture );
						pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   dwTextureStageState[0] );
						pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, dwTextureStageState[1]  );
						pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, dwTextureStageState[2]  );
						pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   dwTextureStageState[3]  );
					}
					
					pd3dDevice->SetRenderState( D3DRS_LIGHTING, dwRenderState[0] );
					pd3dDevice->SetRenderState( D3DRS_AMBIENT, dwRenderState[1] );
					pd3dDevice->SetRenderState( D3DRS_CULLMODE, dwRenderState[2] );
					pd3dDevice->SetRenderState( D3DRS_ZENABLE, dwRenderState[3] );

					pd3dDevice->SetLight(0, &light);
					pd3dDevice->SetMaterial(&mtrl);

					pd3dDevice->SetTransform(D3DTS_WORLD, &mat[0]);
					pd3dDevice->SetTransform(D3DTS_VIEW, &mat[1]);
					pd3dDevice->SetTransform(D3DTS_PROJECTION, &mat[2]);

					pd3dDevice->SetFVF(dwFVF);
					pd3dDevice->SetStreamSource(0,  pVB, uOffset, uStride);
				}

			} DXState;
			
			//DXState.Get(g_pd3dDevice);
			{
				if (!g_pVB)
				{
					InitGeo();
				}

				SetupRenderer();
				SetupLights();
				SetupMatrices();
				SetupTextures();

				// Render the vertex buffer contents
				g_pd3dDevice->SetStreamSource( 0, g_pVB, 0, sizeof(CUSTOMVERTEX) );
				g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
				g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 2*50-2 );


			}
			//DXState.Set(g_pd3dDevice);

	#endif
		}
		bool IsValidTexture(void *pTexture)
		{
			if (pTexture == NULL)
			{
				return false;
			}


			LPDIRECT3DTEXTURE9 pD3DTexture = (LPDIRECT3DTEXTURE9)pTexture;

			if (pD3DTexture == NULL)
			{
				return false;
			}
			
			IDirect3DDevice9 * pD3DDevice;
			
			pD3DTexture->GetDevice(&pD3DDevice);

			const bool bValid = g_pd3dDevice == pD3DDevice;

			if (bValid)
			{
				
			}

			return bValid;
		}
		void GetTextureWithID(size_t uID, void **pTexture)
		{
			g_pd3dDevice->GetTexture((DWORD)uID, (IDirect3DBaseTexture9**)pTexture);
		}

		void SetTextureWithID(size_t uID, void *pTexture)
		{
			g_pd3dDevice->SetTexture((DWORD)uID, (IDirect3DBaseTexture9*)pTexture);
		}
		void TestSetTexture(void *pTexture)
		{
			g_pTexture = (LPDIRECT3DTEXTURE9)pTexture;
		}

		void *GetTextureWithOffset(void *pObject, size_t uOffset)
		{
			return Internal::GetItemInObject<void*, uintptr_t>(pObject, uOffset);
		}
		
		void *GetTexture(ITexPic *pTexPic)
		{
			return GetTextureWithOffset(pTexPic, 24);
		}
		
		MODULE_METHOD void SetPrecacheResourcesMode(bool bEnable)
		{
			if (bEnable)
			{
				this->Shutdown();
			}
			else
			{
				//CryModule::aSystem.Get<IRenderer>()->EF_RegisterFogVolume(0,0,CFColor());
				this->Init(CryModule::aSystem.Get<IRenderer>()->EF_Query(EFQ_D3DDevice), CryModule::aSystem.Get<const char>());
			}
		}
	private:
		
		bool FixPath(wchar_t *wBuffer, const wchar_t *szPath)
		{
			return Internal::FixPath(wBuffer, m_swzRootPath, szPath);
		}


		bool LoadTexure(const wchar_t *szPath, IDirect3DTexture9 **pTexture)
		{
			*pTexture = NULL;
			wchar_t wBuffer[MAX_PATH];
			const bool bFixed = FixPath(wBuffer, szPath);

			if( FAILED( D3DXCreateTextureFromFile( g_pd3dDevice, wBuffer, pTexture ) ) )
			{
				if (!bFixed || FAILED(D3DXCreateTextureFromFile( g_pd3dDevice, szPath, pTexture) ))
				{
					return false;
				}
			}

			return (*pTexture) != NULL;
		}

		void InitGeo()
		{ 
			
			
			// Use D3DX to create a texture from a file based image
			if(!LoadTexure(L"Textures\\banana.bmp", &g_pTexture ))
			{
				MessageBox(NULL, L"Could not find banana.bmp", L"DXRenderer", MB_OK);
				
				return;
			}

			// Create the vertex buffer.
			if( FAILED( g_pd3dDevice->CreateVertexBuffer( 50*2*sizeof(CUSTOMVERTEX),
														0, D3DFVF_CUSTOMVERTEX,
														D3DPOOL_DEFAULT, &g_pVB, NULL ) ) )
			{
				return;
			}

			// Fill the vertex buffer. We are setting the tu and tv texture
			// coordinates, which range from 0.0 to 1.0
			CUSTOMVERTEX* pVertices;
			if( FAILED( g_pVB->Lock( 0, 0, (void**)&pVertices, 0 ) ) )
				return ;
			for( DWORD i=0; i<50; i++ )
			{
				FLOAT theta = (2*D3DX_PI*i)/(50-1);

				pVertices[2*i+0].position = D3DXVECTOR3( sinf(theta),-1.0f, cosf(theta) );
				pVertices[2*i+0].normal   = D3DXVECTOR3( cosf(theta), 0.0f,  sinf(theta));
				pVertices[2*i+0].tu       = ((FLOAT)i)/(50-1);
				pVertices[2*i+0].tv       = 1.0f;

				pVertices[2*i+1].position = D3DXVECTOR3( sinf(theta), 1.0f, cosf(theta) );
				pVertices[2*i+1].normal   = D3DXVECTOR3( cosf(theta) , 0.0f, sinf(theta));
				pVertices[2*i+1].tu       = ((FLOAT)i)/(50-1);
				pVertices[2*i+1].tv       = 0.0f;
			}
			g_pVB->Unlock();

			return;
		}

		void SetupMatrices()
		{

			// Set up world matrix
			D3DXMATRIXA16 matWorld;
			D3DXMatrixIdentity( &matWorld );
			D3DXMatrixRotationX( &matWorld, timeGetTime()/1000.0f );
			g_pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );

			// Set up our view matrix. A view matrix can be defined given an eye point,
			// a point to lookat, and a direction for which way is up. Here, we set the
			// eye five units back along the z-axis and up three units, look at the
			// origin, and define "up" to be in the y-direction.
			D3DXVECTOR3 vEyePt( 0.0f, 3.0f,-5.0f );
			D3DXVECTOR3 vLookatPt( 0.0f, 0.0f, 0.0f );
			D3DXVECTOR3 vUpVec( 0.0f, 1.0f, 0.0f );
			D3DXMATRIXA16 matView;
			D3DXMatrixLookAtLH( &matView, &vEyePt, &vLookatPt, &vUpVec );
			g_pd3dDevice->SetTransform( D3DTS_VIEW, &matView );

			// For the projection matrix, we set up a perspective transform (which
			// transforms geometry from 3D view space to 2D viewport space, with
			// a perspective divide making objects smaller in the distance). To build
			// a perpsective transform, we need the field of view (1/4 pi is common),
			// the aspect ratio, and the near and far clipping planes (which define at
			// what distances geometry should be no longer be rendered).
			D3DXMATRIXA16 matProj;
			D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, 1.0f, 1.0f, 100.0f );
			g_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );
		}

		void SetupLights()
		{
			

			// Set up a material. The material here just has the diffuse and ambient
			// colors set to yellow. Note that only one material can be used at a time.
			D3DMATERIAL9 mtrl;
			ZeroMemory( &mtrl, sizeof(D3DMATERIAL9) );
			mtrl.Diffuse.r = mtrl.Ambient.r = 1.0f;
			mtrl.Diffuse.g = mtrl.Ambient.g = 1.0f;
			mtrl.Diffuse.b = mtrl.Ambient.b = 1.0f;
			mtrl.Diffuse.a = mtrl.Ambient.a = 1.0f;
			g_pd3dDevice->SetMaterial( &mtrl );

			// Set up a white, directional light, with an oscillating direction.
			// Note that many lights may be active at a time (but each one slows down
			// the rendering of our scene). However, here we are just using one. Also,
			// we need to set the D3DRS_LIGHTING renderstate to enable lighting
			D3DXVECTOR3 vecDir;
			D3DLIGHT9 light;
			ZeroMemory( &light, sizeof(D3DLIGHT9) );
			light.Type       = D3DLIGHT_DIRECTIONAL;
			light.Diffuse.r  = 1.0f;
			light.Diffuse.g  = 1.0f;
			light.Diffuse.b  = 1.0f;
			vecDir = D3DXVECTOR3(1.0f,
								0.0f,
								0.0f );
			D3DXVec3Normalize( (D3DXVECTOR3*)&light.Direction, &vecDir );
			light.Range       = 1000.0f;
			g_pd3dDevice->SetLight( 0, &light );
			g_pd3dDevice->LightEnable( 0, TRUE );
			g_pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );

			// Finally, turn on some ambient light.
			g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, 0x00202020 );
		}
		void SetupTextures()
		{
			// Setup our texture. Using textures introduces the texture stage states,
			// which govern how textures get blended together (in the case of multiple
			// textures) and lighting information. In this case, we are modulating
			// (blending) our texture with the diffuse color of the vertices.
			
			IDirect3DTexture9 *p = g_pTextTexture;
			g_pd3dDevice->SetTexture( 0, p );
			g_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
			g_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
			g_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
			g_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_DISABLE );
		}
		void SetupRenderer()
		{
			//Clear();

			g_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
			g_pd3dDevice->SetRenderState( D3DRS_ZENABLE, TRUE );
		}

		wchar_t m_swzRootPath[MAX_PATH];
	};// UtilsPluginClassRegister(CRenderer);

	
};


/*
bool GfxDeviceD3D9::ReadbackImage( )
{
    HRESULT hr;
    IDirect3DDevice9* dev = GetD3DDevice();
    SurfacePointer renderTarget;
    hr = dev->GetRenderTarget( 0, &renderTarget );
    if( !renderTarget || FAILED(hr) )
        return false;

    D3DSURFACE_DESC rtDesc;
    renderTarget->GetDesc( &rtDesc );

    SurfacePointer resolvedSurface;
    if( rtDesc.MultiSampleType != D3DMULTISAMPLE_NONE )
    {
        hr = dev->CreateRenderTarget( rtDesc.Width, rtDesc.Height, rtDesc.Format, D3DMULTISAMPLE_NONE, 0, FALSE, &resolvedSurface, NULL );
        if( FAILED(hr) )
            return false;
        hr = dev->StretchRect( renderTarget, NULL, resolvedSurface, NULL, D3DTEXF_NONE );
        if( FAILED(hr) )
            return false;
        renderTarget = resolvedSurface;
    }

    SurfacePointer offscreenSurface;
    hr = dev->CreateOffscreenPlainSurface( rtDesc.Width, rtDesc.Height, rtDesc.Format, D3DPOOL_SYSTEMMEM, &offscreenSurface, NULL );
    if( FAILED(hr) )
        return false;

    hr = dev->GetRenderTargetData( renderTarget, offscreenSurface );
    bool ok = SUCCEEDED(hr);
    if( ok )
    {
        // Here we have data in offscreenSurface.
        D3DLOCKED_RECT lr;
        RECT rect;
        rect.left = 0;
        rect.right = rtDesc.Width;
        rect.top = 0;
        rect.bottom = rtDesc.Height;
        // Lock the surface to read pixels
        hr = offscreenSurface->LockRect( &lr, &rect, D3DLOCK_READONLY );
        if( SUCCEEDED(hr) )
        {
            // Pointer to data is lt.pBits, each row is
            // lr.Pitch bytes apart (often it is the same as width*bpp, but
            // can be larger if driver uses padding)

            // Read the data here!
            offscreenSurface->UnlockRect();
        }
        else
        {
            ok = false;
        }
    }

    return ok;
}
*/
 /*
 	if (CRenderer::CV_r_envcmwrite)
 	{
 		for(n=Start; n<End; n++)
 		{ 
 		static char* cubefaces[6] = {"posx","negx","posy","negy","posz","negz"};
 		char str[64];
 		int width = tex_size;
 		int height = tex_size;
 		byte *pic = new byte [width * height * 4];
 		LPDIRECT3DSURFACE9 pSysSurf, pTargetSurf;
 		LPDIRECT3DTEXTURE9 pID3DSysTexture;
 		D3DLOCKED_RECT d3dlrSys;
 		if (!pID3DTargetTextureCM)
 		pTargetSurf = (LPDIRECT3DSURFACE9)pEnv->m_pRenderTargets[n];
 		else
 		h = pID3DTargetTextureCM->GetCubeMapSurface((D3DCUBEMAP_FACES)n, 0, &pTargetSurf);
 		h = D3DXCreateTexture(r->GetDevice(), tex_size, tex_size, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, &pID3DSysTexture);
 		h = pID3DSysTexture->GetSurfaceLevel(0, &pSysSurf);
 		if (pID3DTargetTexture)
 		h = D3DXLoadSurfaceFromSurface(pSysSurf, NULL, NULL, pTargetSurf, NULL, NULL,  D3DX_FILTER_NONE, 0);
 		else
 		h = r->g_pd3dDevice->GetRenderTargetData(pTargetSurf, pSysSurf);
 		h = pID3DSysTexture->LockRect(0, &d3dlrSys, NULL, 0);
 		byte *src = (byte *)d3dlrSys.pBits;
 		for (int i=0; i<width*height; i++)
 		{
 		*(uint32 *)&pic[i*4] = *(uint32 *)&src[i*4];
 		Exchange(pic[i*4+0], pic[i*4+2]);
 		}
 		h = pID3DSysTexture->UnlockRect(0);
 		sprintf(str, "Cube_%s.jpg", cubefaces[n]);
 		WriteJPG(pic, width, height, str, 24); 
 		delete [] pic;
 		if (pID3DTargetTextureCM)
 		SAFE_RELEASE (pTargetSurf);
 		SAFE_RELEASE (pSysSurf);
 		SAFE_RELEASE (pID3DSysTexture);
 		}
 		CRenderer::CV_r_envcmwrite = 0;
 	}
 */