#pragma once


#include <DXUT.h>
#include "DXInternal.h"
#include <Windows.h>
#include <mmsystem.h>
#include <d3dx9.h>
#include <strsafe.h>
#pragma warning( default : 4996 ) 

struct ITexPic;
struct IRenderer;

extern LPDIRECT3DDEVICE9  g_pid3dDevice;
extern  IDirect3DDevice9 *g_pd3dDevice;
extern  LPDIRECT3DTEXTURE9 g_pTextTexture;

ITexPic *CreateTexture(int w, int h, char *szCacheName);

inline void CopySurface(IDirect3DSurface9* pSrc, IDirect3DSurface9* pDst)
{
	g_pd3dDevice->StretchRect(pSrc, NULL, pDst, NULL, D3DTEXF_NONE);
}

LPDIRECT3DTEXTURE9 GetTexture(ITexPic *pTexPic);
LPDIRECT3DDEVICE9EX GetDevice(IRenderer* pRenderer);
LPDIRECT3DDEVICE9EX GetDevice();

const char *GetD3DErrorType(HRESULT aType);
